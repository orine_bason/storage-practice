import { v4 as uuidv4 } from 'uuid';
import _ from 'lodash';

function findFunc(object) {
    return true; 
}

class storage{
    savedData = [];

    constructor(initData){
        this.savedData = initData;
    }

    create(item) {
        let newId = uuidv4();
        item.id = newId;
        return item.keys;
    };
    
    find(item, findfunc){
        if(findFunc(item)){
            return this.savedData.keys;
        }
    }

    getAll() {
        return this.savedData.entries;
    }

    remove(item, findfunc ){
        let removedItems = [];
        this.savedData.forEach( data => {
            if(!findfunc(data)){
                this.savedData.remove(data)
                removedItems.push(data);
            }
            return removedItems.keys;
    })
    }

    where(whereObj){
        let searchedData = []
        this.savedData.forEach(data =>{
        if( _.find(data, whereObj) != null){
                searchedData.push(data);
        }
        return searchedData;
        })}
    }

module.exports =  {where, create, find, remove, getAll}